import './App.css';
import Home from './components/Home';
import Login from './components/Login';
import Registration from './components/Registration';
import {
  Route,
  Link,
  Routes,
  BrowserRouter
} from "react-router-dom";
import NoContent from './components/NoContent';
import Viewer from './components/Viewer';

const users = [
  { username: 'Афанасий', login: 'User', password: '1' }
];

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p> 
          <BrowserRouter>
            <Routes>
              <Route path='/' element={<Home />} />
              {/* подстановочный путь */}
              <Route path="login" element={<Login />} />
              <Route path="registration" element={<div><Registration user={users[0]} /><br/><Viewer /></div>} />
              <Route path='*' element={<NoContent />} />
            </Routes>
          </BrowserRouter>
        </p>
      </header>
    </div>
  );
}

export default App;
