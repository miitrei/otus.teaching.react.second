import { User } from "../models/user";

export const Actions = {
    setUser: '[USER_STATE] USER_SET'
};

interface State {
    user: User | null;
}

const initialState: State = {
    user: null
}

const userReducer = (state = initialState, action: any) => {
    console.log(action);
    switch (action.type) {
        case '[USER_STATE] USER_SET':
            alert(`${state.user?.username} stored!`);
            return { ...state, user: action.payload };
        default:
            return state;
    }
};

export default userReducer;