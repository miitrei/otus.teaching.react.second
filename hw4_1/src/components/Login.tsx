import { Button, Link, TextField } from "@mui/material";
import React, { useState } from 'react';

function Login()
{    
    // Urls
    const url = "https://catfact.ninja/fact"; 
    
    // Styles
    const authorizationFormStyle = {
        "padding": "50px"
    };
    const entryStyle = {
        "backgroundColor": "whitesmoke",
        "marginTop": "20px"
    };
    const buttonStyle = {
        "marginTop": "20px"
    };
    const linkStyle = {
        "marginTop": "5px"
    };
    
    // HOCs
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [cat, setCat] = useState([]);

    // Properties On Change
    const onChangeLogin = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        setLogin(newValue);
    }
    const onChangePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        setPassword(newValue);
    }

    // Actions
    const getCats = async () => {
        try {
          const response = await fetch(url);
          const cat = await response.json()
          setCat(cat.fact);
        } 
        catch (error) {
          console.log(error)
        }
    };

    async function handleClick() {
        await getCats();

        if (login === 'User'
            && password === '1')
            alert(`Success! Get your cat fact:\n${cat}`);
        else
            alert(`Wrong login or password! Login: ${login}, Password: ${password}`);
      }
    
    return (        
        <form style={authorizationFormStyle} >
            <div>
                <label>Login:</label>
            </div>
            <br />

            <TextField 
                style={entryStyle}
                onChange={onChangeLogin} 
                label="Login" 
                variant="filled" />
            <br />

            <TextField
                style={entryStyle}
                onChange={onChangePassword}
                label="Password"
                type="password"
                autoComplete="current-password" 
                variant="filled" />
            <br />

            <Button
                style={buttonStyle}
                variant={'contained'} 
                onClick={handleClick}>Login</Button>  
            <br />

            <Link
                style={linkStyle}
                variant="body2"
                href="/registration">
                Registration
            </Link>     
            <br/>            
        </form>        
    );
}

export default Login;
