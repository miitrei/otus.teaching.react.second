import { useSelector } from "react-redux";

const Viewer = () => {    
    const currentUser = useSelector((gs:any) => gs?.userStorage?.user?.username);

    const span = {
        display: 'inline'
    };

    return <>
        <div style={span}>Result name: </div>
        <div style={span}>{currentUser}</div>
    </>;
}

export default Viewer;
