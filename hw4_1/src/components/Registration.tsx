import {Button, Link, TextField} from "@mui/material";
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import React, { useState } from 'react';
import dayjs, { Dayjs } from "dayjs";
import { useDispatch } from "react-redux";
import { User } from '../models/user';

interface Props {
    user: User;
}

const Registration = (props: Props) => {    

    // Styles
    const registrationFormStyle = {
        "padding": "50px"
    };
    const entryStyle = {
        "backgroundColor": "whitesmoke",
        "marginTop": "20px",
        "minWidth": "260px"
    };
    const dateStyle = {
        "backgroundColor": "whitesmoke",
        "marginTop": "20px"
    };
    const buttonStyle = {
        "marginTop": "20px"
    };
    
    // Redux Init
    const { user } = props;
    const dispatch = useDispatch();

    // HOCs
    const [login, setLogin] = useState('');
    const [name, setName] = useState('');
    const [birthDate, setBirthDate] = useState<Dayjs | null>(null);
    const [password, setPassword] = useState('');

    // Properties On Change
    const onChangeLogin = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        setLogin(newValue);
    }
    const onChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        setName(newValue);
    }
    const onChangePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        setPassword(newValue);
    }

    // Actions
    const reduceAndRegister = () => {
        if (login === '' 
            || password === ''
            || name === '')
        {
            alert('Empty user data!'); 
            return;
        }

        user.login = login;
        user.password = password;
        user.username = name;

        dispatch({ type: '[USER_STATE] USER_SET', payload: user });
        const userName = dispatch({ type: '[USER_STATE] USER_SET', payload: user });

        alert(`${userName.payload.username} saved!`);
    };

    // Render
    return (        
        <form style={registrationFormStyle} >
            <div>
                <label>Registration:</label>
            </div>
            <br />

            <TextField 
                label="Login" 
                onChange={onChangeLogin} 
                required
                style={entryStyle}
                variant="filled" />
            <br />

            <TextField 
                label="Name"
                onChange={onChangeName}
                required
                style={entryStyle}
                variant="filled" />
            <br />

            <TextField 
                autoComplete="current-password"
                label="Password"
                onChange={onChangePassword}
                required
                style={entryStyle}
                type="password"
                variant="filled" />
            <br />

            <div style={dateStyle}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker 
                        onChange={setBirthDate}
                        value={birthDate} />
                </LocalizationProvider>
            </div>

            <Button 
                onClick={reduceAndRegister}
                style={buttonStyle}
                variant={'contained'}>
                    Save
            </Button>  
            <br/>       

            <Link className="link_field"
                    variant="body2"
                    href="/login">
                    Log In Page
            </Link>     
            <br/>    
        </form>              
    );
}

export default Registration;
