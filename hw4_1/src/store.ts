import { configureStore } from '@reduxjs/toolkit';
import userReducer from './store/userReducer';

const store = configureStore({
    reducer: {
        userStorage: userReducer,
    }
});

export default store;